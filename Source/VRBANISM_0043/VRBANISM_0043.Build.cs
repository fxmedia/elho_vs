// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class VRBANISM_0043 : ModuleRules
{
	public VRBANISM_0043(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] {
            // Default dependencies
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            
            // Added UE4 dependencies
            "Json",  // Parse Websocket and API data
            "JsonUtilities",  // Parse Websocket and API data
            "Http",  // Make API calls
            "UMG",  // Add WidgetComponents to Invitee
            "MediaAssets",  // Needed to import from NDIIO

            // Added plugin dependencies
            "BlueprintWebSocket",
            "NDIIO",
        });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
