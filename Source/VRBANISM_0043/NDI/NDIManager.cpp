// Fill out your copyright notice in the Description page of Project Settings.


#include "NDIManager.h"

// Sets default values
ANDIManager::ANDIManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANDIManager::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ANDIManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

