// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CarouselObject.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "CarouselManager.generated.h"

UCLASS()
class VRBANISM_0043_API ACarouselManager : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	// Array of all carousel objects in the level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CarouselObject)
	TArray<ACarouselObject*> Objects;

public:  // Public functions
	// Sets default values for this actor's properties
	ACarouselManager();

	// Update the visibility of all carousel objects
	UFUNCTION(BlueprintCallable)
	void UpdateObject(FReplyLiveObjectData data);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
