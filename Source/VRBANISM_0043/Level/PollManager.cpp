// Fill out your copyright notice in the Description page of Project Settings.


#include "PollManager.h"

// Sets default values
APollManager::APollManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APollManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APollManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APollManager::StartPoll(FReplyLivePollData PollData)
{
	if (PollObject) {
		// First clear any possible current Polls
		PollObject->ExitPoll(PollData, false);

		// Then start the new one
		PollObject->InitPoll(PollData);
	}
}

void APollManager::PauzePoll(FReplyLivePollData PollData)
{
	if (PollObject) {
		PollObject->ExitPoll(PollData, true);
	}
}

void APollManager::StopPoll(FReplyLivePollData PollData)
{
	if (PollObject) {
		PollObject->ExitPoll(PollData, false);
	}
}

void APollManager::ClearPoll(FReplyLivePollData PollData)
{
	if (PollObject) {
		PollObject->ExitPoll(PollData, false);
	}
}

void APollManager::GetPollAnswers(FReplyLiveAPIPollAnswers PollAnswers) 
{
	if (PollObject) {
		PollObject->UpdatePoll(PollAnswers);
	}
}