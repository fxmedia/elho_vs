// Fill out your copyright notice in the Description page of Project Settings.


#include "InviteeEmojiWidget.h"

void UInviteeEmojiWidget::SetEmoji_Implementation(const FString & Emoji)
{
	UE_LOG(LogTemp, Log, TEXT("UInviteeEmojiWidget::SetEmoji Executing in C++"));
}

void UInviteeEmojiWidget::HideEmoji_Implementation()
{
	UE_LOG(LogTemp, Log, TEXT("UInviteeEmojiWidget::HideEmoji Executing in C++"));
}

void UInviteeEmojiWidget::ExecuteAnimation_Implementation()
{
	UE_LOG(LogTemp, Log, TEXT("UInviteeEmojiWidget::ExecuteAnimation Executing in C++"));
}