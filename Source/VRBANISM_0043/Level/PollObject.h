// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "PollObject.generated.h"

UCLASS()
class VRBANISM_0043_API APollObject : public AActor
{
	GENERATED_BODY()

public:  // Public variables
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void InitPoll(FReplyLivePollData PollData);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void UpdatePoll(FReplyLiveAPIPollAnswers PollAnswers);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ExitPoll(FReplyLivePollData PollData, bool KeepVisibile);

public:	// Public functions
	// Sets default values for this actor's properties
	APollObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
